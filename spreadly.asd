;;;; -*- coding: utf-8 -*-

(asdf:defsystem #:spreadly :description
 "The Gendl® spreadly Subsystem" :author "Genworks International"
 :license "Affero Gnu Public License (http://www.gnu.org/licenses/)"
 :serial t :version "20201015" :depends-on nil :defsystem-depends-on
 nil :components
 ((:file "source/package") (:gdl "source/assembly")
  (:gdl "source/initialize")))
